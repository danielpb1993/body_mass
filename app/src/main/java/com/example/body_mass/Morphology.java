package com.example.body_mass;

public enum Morphology {
    SMALL, MEDIUM, BROAD
}
