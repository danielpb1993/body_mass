package com.example.body_mass;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroupType;
    private RadioGroup radioGroupGender;
    private RadioGroup radioGroupMorph;

    private RadioButton radioBtFat;
    private RadioButton radioBtWan;
    private RadioButton radioBtLorentz;
    private RadioButton radioBtCreff;
    private RadioButton radioBtMale;
    private RadioButton radioBtFemale;
    private RadioButton radioBtSmall;
    private RadioButton radioBtMedium;
    private RadioButton radioBtBroad;

    private TextInputEditText textHeight;
    private TextInputEditText textWaist;
    private TextInputEditText textAge;
    private TextInputLayout textWaistLay;
    private TextInputLayout textAgeLay;

    private TextView textResult;
    private LinearLayout linearTable;

    private Button btCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroupType = findViewById(R.id.radioGroupType);
        radioGroupGender = findViewById(R.id.gender);
        radioGroupMorph = findViewById(R.id.morphology);

        radioBtFat = findViewById(R.id.radioBtFat);
        radioBtWan = findViewById(R.id.radioBtWan);
        radioBtLorentz = findViewById(R.id.radioBtLorentz);
        radioBtCreff = findViewById(R.id.radioBtCreff);
        radioBtMale = findViewById(R.id.radioBtMale);
        radioBtFemale = findViewById(R.id.radioBtFemale);
        radioBtSmall = findViewById(R.id.radioBtSmall);
        radioBtMedium = findViewById(R.id.radioBtMedium);
        radioBtBroad = findViewById(R.id.radioBtBroad);

        textHeight = findViewById(R.id.textHeight);
        textWaist = findViewById(R.id.textWaist);
        textAge = findViewById(R.id.textAge);
        textWaistLay = findViewById(R.id.textWaistLay);
        textAgeLay = findViewById(R.id.textAgeLay);

        textResult = findViewById(R.id.textResult);
        linearTable = findViewById(R.id.linearTable);

        btCalculate = findViewById(R.id.btCalculate);

        textHeight.setVisibility(View.GONE);
        radioGroupGender.setVisibility(View.GONE);
        radioGroupMorph.setVisibility(View.GONE);
        textWaistLay.setVisibility(View.GONE);
        textAgeLay.setVisibility(View.GONE);
        btCalculate.setVisibility(View.GONE);
        linearTable.setVisibility(View.GONE);

        radioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                textHeight.setVisibility(View.VISIBLE);
                btCalculate.setVisibility(View.VISIBLE);
                int radioSelected = radioGroupType.getCheckedRadioButtonId();
                switch (radioSelected) {
                    case R.id.radioBtFat:
                        linearTable.setVisibility(View.VISIBLE);
                        radioGroupGender.setVisibility(View.VISIBLE);
                        radioGroupMorph.setVisibility(View.GONE);
                        textWaistLay.setVisibility(View.VISIBLE);
                        textAgeLay.setVisibility(View.GONE);
                        break;

                    case R.id.radioBtWan:
                        linearTable.setVisibility(View.GONE);
                        radioGroupGender.setVisibility(View.VISIBLE);
                        radioGroupMorph.setVisibility(View.GONE);
                        textAgeLay.setVisibility(View.GONE);
                        textWaistLay.setVisibility(View.GONE);
                        break;

                    case R.id.radioBtLorentz:
                        linearTable.setVisibility(View.GONE);
                        radioGroupGender.setVisibility(View.VISIBLE);
                        radioGroupMorph.setVisibility(View.GONE);
                        textAgeLay.setVisibility(View.VISIBLE);
                        textWaistLay.setVisibility(View.GONE);
                        break;

                    case R.id.radioBtCreff:
                        linearTable.setVisibility(View.GONE);
                        radioGroupMorph.setVisibility(View.VISIBLE);
                        radioGroupGender.setVisibility(View.GONE);
                        textAgeLay.setVisibility(View.VISIBLE);
                        textWaistLay.setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }
            }
        });

        btCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioBtFat.isChecked() && radioBtMale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textWaist.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opWaist = Integer.parseInt(textWaist.getText().toString().trim());
                        textResult.setText(BodyMass.RFM(Gender.MALE, opHeight, opWaist) + "%");
                    }
                }
                if (radioBtFat.isChecked() && radioBtFemale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textWaist.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opWaist = Integer.parseInt(textWaist.getText().toString().trim());
                        textResult.setText(BodyMass.RFM(Gender.FEMALE, opHeight, opWaist) + "%");
                    }
                }
                if (radioBtWan.isChecked() && radioBtMale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        textResult.setText(BodyMass.WV(Gender.MALE, opHeight) + "Kg");
                    }
                }
                if (radioBtWan.isChecked() && radioBtFemale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        textResult.setText(BodyMass.WV(Gender.FEMALE, opHeight) + "Kg");
                    }
                }
                if (radioBtLorentz.isChecked() && radioBtMale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textAge.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opAge = Integer.parseInt(textAge.getText().toString().trim());
                        textResult.setText(BodyMass.L(Gender.MALE, opHeight, opAge) + "Kg");
                    }
                }
                if (radioBtLorentz.isChecked() && radioBtFemale.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textAge.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opAge = Integer.parseInt(textAge.getText().toString().trim());
                        textResult.setText(BodyMass.L(Gender.FEMALE, opHeight, opAge) + "Kg");
                    }
                }
                if (radioBtCreff.isChecked() && radioBtSmall.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textAge.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opAge = Integer.parseInt(textAge.getText().toString().trim());
                        textResult.setText(BodyMass.C(Morphology.SMALL, opHeight, opAge) + "Kg");
                    }
                }
                if (radioBtCreff.isChecked() && radioBtMedium.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textAge.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opAge = Integer.parseInt(textAge.getText().toString().trim());
                        textResult.setText(BodyMass.C(Morphology.MEDIUM, opHeight, opAge) + "Kg");
                    }
                }
                if (radioBtCreff.isChecked() && radioBtBroad.isChecked()) {
                    if (textHeight.getText().toString().trim().isEmpty() || textAge.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show();
                    } else {
                        int opHeight = Integer.parseInt(textHeight.getText().toString().trim());
                        int opAge = Integer.parseInt(textAge.getText().toString().trim());
                        textResult.setText(BodyMass.C(Morphology.BROAD, opHeight, opAge) + "Kg");
                    }
                }
            }
        });
    }
}