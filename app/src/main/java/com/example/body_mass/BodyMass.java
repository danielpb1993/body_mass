package com.example.body_mass;

public class BodyMass {
    public static double RFM (Gender gender, int altura, int cc){
        double result = 0;
        if (gender == Gender.MALE) {
            result = 64 - (20 * altura / cc);
        }
        if (gender == Gender.FEMALE) {
            result = 76 - (20 * altura / cc);
        }
        return result;
    }

    public static double WV (Gender gender, int altura) {
        double result = 0;
        if (gender == Gender.MALE) {
            result = (altura - 150) * 0.75 + 50;
        }
        if (gender == Gender.FEMALE) {
            result = (altura - 150) * 0.6 + 50;
        }
        return result;
    }

    public static double L (Gender gender, int altura, int edat) {
        double result = 0;
        if (gender == Gender.MALE) {
            result = altura - 100 - ((altura-150)/4) + ((edat-20)/4);
        }
        if (gender == Gender.FEMALE) {
            result = altura - 100 - ((altura-150)/2.5) + ((edat-20)/2.5);
        }
        return result;
    }

    public static double C (Morphology morphology, int altura, int edat) {
        double result = 0;
        if (morphology == Morphology.SMALL) {
            result = Math.round(((altura - 100 + edat/10)*Math.pow(0.9,2))*100.0)/100.0;
        }
        if (morphology == Morphology.MEDIUM) {
            result = Math.round(((altura - 100 + edat/10)* 0.9)*100.0)/100.0;
        }
        if (morphology == Morphology.BROAD) {
            result = Math.round(((altura - 100 + edat/10)* 0.9 * 1.1)*100.0)/100.0;
        }
        return result;
    }
}
